# I. Un premier serveur web

## 1. Installation

La premiere chose à faire est d'installer le paquet httpd
```bash
[welan@web ~]$ sudo dnf install httpd
```

Pour le démarrer : 
```bash
[welan@web ~]$ sudo systemctl start httpd
```

Pour voir si le service est lancé :
```bash
[welan@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
     Active: active (running) since Tue 2022-11-15 11:10:44 CET; 11s ago
       Docs: man:httpd.service(8)
   Main PID: 10798 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
      Tasks: 213 (limit: 5907)
     Memory: 23.1M
        CPU: 88ms
     CGroup: /system.slice/httpd.service
             ├─10798 /usr/sbin/httpd -DFOREGROUND
             ├─10799 /usr/sbin/httpd -DFOREGROUND
             ├─10800 /usr/sbin/httpd -DFOREGROUND
             ├─10801 /usr/sbin/httpd -DFOREGROUND
             └─10802 /usr/sbin/httpd -DFOREGROUND

Nov 15 11:10:43 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Nov 15 11:10:44 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Nov 15 11:10:44 web.tp2.linux httpd[10798]: Server configured, listening on: port 80
```

```bash
[welan@web ~]$ sudo ss -lntp
State          Recv-Q          Send-Q                   Local Address:Port                   Peer Address:Port         Process
LISTEN         0               128                            0.0.0.0:22                          0.0.0.0:*             users:(("sshd",pid=682,fd=3))
LISTEN         0               511                                  *:80                                *:*             users:(("httpd",pid=10802,fd=4),("httpd",pid=10801,fd=4),("httpd",pid=10800,fd=4),("httpd",pid=10798,fd=4))
LISTEN         0               128                               [::]:22                             [::]:*             users:(("sshd",pid=682,fd=4))
```
On peut voir que le port qu'utilise Apache est le 80

Pour l'ouvrir :
```bash
[welan@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

```bash
PS C:\Users\Raphaël> curl 10.102.1.11:80
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system.
If you can read this page, it means that the software it working correctly.
Just visiting?
```
On peut voir que le service fonctionne donc bien

## 2. Avancer vers la maîtrise du service

```bash
[welan@web ~]$ sudo systemctl cat httpd
[sudo] password for welan:
# /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
```
Cette commande nous permet de voir la définition du service Apache

```bash
ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache


ServerAdmin root@localhost
```
On peut voir que le user qui est utilisé est Apache

```bash
ps -ef | grep httpd
root       10798       1  0 11:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     10799   10798  0 11:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     10800   10798  0 11:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     10801   10798  0 11:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     10802   10798  0 11:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```
Ici on peut voir ques les utilisateurs utilisés sont apache et root

```bash
[welan@web ~]$ ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Nov 15 10:50 .
drwxr-xr-x. 81 root root 4096 Nov 15 10:50 ..
-rw-r--r--.  1 root root 7620 Jul  6 04:37 index.html
```
On peut voir que le user apache n'appartient pas au groupe root et n'a donc que le droit de lecture

Pour créer un nouvel utilisateur :
```bash
[welan@web ~]$ sudo useradd pyke --home /usr/share/httpd -s /sbin/login
```

Ensuite on modifie le fichier conf pour changer le user utilisé
```bash
ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User pyke
Group apache


ServerAdmin root@localhost
```

Maintenant on redémarre le service
```bash
[welan@web ~]$ ps -ef | grep httpd
root       11253       1  0 11:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
pyke       11254   11253  0 11:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
pyke       11255   11253  0 11:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
pyke       11256   11253  0 11:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
pyke       11257   11253  0 11:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```
Ici on peut voir que le user utilisé pour le service est bien pyke

Pour changer le port d'écoute du serveur il faut de nouveau ouvrir le fichier conf et changer la ligne Listen
```bash
ServerRoot "/etc/httpd"

Listen 8080

Include conf.modules.d/*.conf

User pyke
Group apache


ServerAdmin root@localhost
```

Ensuite on ferme l'ancien port et on ouvre le nouveau
```bash
[welan@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[welan@web ~]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
[welan@web ~]$ sudo firewall-cmd --reload
success
```

```bash
[welan@web ~]$ sudo ss -lntp
State              Recv-Q             Send-Q                         Local Address:Port                           Peer Address:Port             Process
LISTEN             0                  128                                  0.0.0.0:22                                  0.0.0.0:*                 users:(("sshd",pid=682,fd=3))
LISTEN             0                  511                                        *:8080                                      *:*                 users:(("httpd",pid=11516,fd=4),("httpd",pid=11515,fd=4),("httpd",pid=11514,fd=4),("httpd",pid=11512,fd=4))
LISTEN             0                  128                                     [::]:22                                     [::]:*                 users:(("sshd",pid=682,fd=4))
```
On peut voir ici que le port d'écoute est bien 8080

```bash
PS C:\Users\Raphaël> curl 10.102.1.11:8080
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system.
If you can read this page, it means that the software it working correctly.
Just visiting?
```
Le service fonctionne donc bien

# II. Une stack web plus avancée
## A. Base de données
Pour installer mariadb nous avons plusieurs étapes à réaliser

``bash
[welan@db ~]$ sudo dnf install mariadb-server
```

Apres l'installation :
```bash
[welan@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
[welan@db ~]$ sudo systemctl start mariadb
[welan@db ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] n
 ... skipping.

You already have your root account protected, so you can safely answer 'n'.

Change the root password? [Y/n] n
 ... skipping.

By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```bash

```bash
[welan@db ~]$ sudo ss -lntp
State     Recv-Q    Send-Q       Local Address:Port       Peer Address:Port    Process
LISTEN    0         128                0.0.0.0:22              0.0.0.0:*        users:(("sshd",pid=710,fd=3))
LISTEN    0         80                       *:3306                  *:*        users:(("mariadbd",pid=12811,fd=19))
LISTEN    0         128                   [::]:22                 [::]:*        users:(("sshd",pid=710,fd=4))
```

Pour ouvrir le bon port:
```bash
[welan@web ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[welan@web ~]$ sudo firewall-cmd --reload
success
```

```bash
[welan@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 10
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.003 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```

Sur l'autre machine :
```bash
[welan@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 12
Server version: 5.5.5-10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> USE nextcloud;
Database changed
mysql> SHOW TABLES;
Empty set (0.00 sec)
```

Pour voir tout les Users : 
```bash
MariaDB [(none)]> SELECT * FROM mysql.user;
+-------------+-------------+-------------------------------------------+-------------+-------------+-------------+-------------+-------------+-----------+-------------+---------------+--------------+-----------+------------+-----------------+------------+------------+--------------+------------+-----------------------+------------------+--------------+-----------------+------------------+------------------+----------------+---------------------+--------------------+------------------+------------+--------------+------------------------+---------------------+----------+------------+-------------+--------------+---------------+-------------+-----------------+----------------------+-----------------------+-------------------------------------------+------------------+---------+--------------+--------------------+
| Host        | User        | Password                                  | Select_priv | Insert_priv | Update_priv | Delete_priv | Create_priv | Drop_priv | Reload_priv | Shutdown_priv | Process_priv | File_priv | Grant_priv | References_priv | Index_priv | Alter_priv | Show_db_priv | Super_priv | Create_tmp_table_priv | Lock_tables_priv | Execute_priv | Repl_slave_priv | Repl_client_priv | Create_view_priv | Show_view_priv | Create_routine_priv | Alter_routine_priv | Create_user_priv | Event_priv | Trigger_priv | Create_tablespace_priv | Delete_history_priv | ssl_type | ssl_cipher | x509_issuer | x509_subject | max_questions | max_updates | max_connections | max_user_connections | plugin                | authentication_string                     | password_expired | is_role | default_role | max_statement_time |
+-------------+-------------+-------------------------------------------+-------------+-------------+-------------+-------------+-------------+-----------+-------------+---------------+--------------+-----------+------------+-----------------+------------+------------+--------------+------------+-----------------------+------------------+--------------+-----------------+------------------+------------------+----------------+---------------------+--------------------+------------------+------------+--------------+------------------------+---------------------+----------+------------+-------------+--------------+---------------+-------------+-----------------+----------------------+-----------------------+-------------------------------------------+------------------+---------+--------------+--------------------+
| localhost   | mariadb.sys |                                           | N
| N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N
 | N                      | N                   |          |            |
|              |             0 |           0 |               0 |                    0 | mysql_native_password |                                           | Y
 | N       |              |           0.000000 |
| localhost   | root        | invalid                                   | Y
| Y           | Y           | Y           | Y           | Y         | Y           | Y             | Y            | Y         | Y          | Y               | Y          | Y          | Y            | Y          | Y                     | Y                | Y            | Y               | Y                | Y                | Y              | Y                   | Y                  | Y                | Y          | Y
 | Y                      | Y                   |          |            |
|              |             0 |           0 |               0 |                    0 | mysql_native_password | invalid                                   | N
 | N       |              |           0.000000 |
| localhost   | mysql       | invalid                                   | Y
| Y           | Y           | Y           | Y           | Y         | Y           | Y             | Y            | Y         | Y          | Y               | Y          | Y          | Y            | Y          | Y                     | Y                | Y            | Y               | Y                | Y                | Y              | Y                   | Y                  | Y                | Y          | Y
 | Y                      | Y                   |          |            |
|              |             0 |           0 |               0 |                    0 | mysql_native_password | invalid                                   | N
 | N       |              |           0.000000 |
| 10.102.1.11 | nextcloud   | *AF136CF35F0D546F69717A7F18C18849666E64D0 | N
| N           | N           | N           | N           | N         | N           | N             | N            | N         | N          | N               | N          | N          | N            | N          | N                     | N                | N            | N               | N                | N                | N              | N                   | N                  | N                | N          | N
 | N                      | N                   |          |            |
|              |             0 |           0 |               0 |                    0 | mysql_native_password | *AF136CF35F0D546F69717A7F18C18849666E64D0 | N
 | N       |              |           0.000000 |
+-------------+-------------+-------------------------------------------+-------------+-------------+-------------+-------------+-------------+-----------+-------------+---------------+--------------+-----------+------------+-----------------+------------+------------+--------------+------------+-----------------------+------------------+--------------+-----------------+------------------+------------------+----------------+---------------------+--------------------+------------------+------------+--------------+------------------------+---------------------+----------+------------+-------------+--------------+---------------+-------------+-----------------+----------------------+-----------------------+-------------------------------------------+------------------+---------+--------------+--------------------+
4 rows in set (0.001 sec)
```
On peut voir que le user nextcloud existe

```bash
[welan@web ~]$ dnf module list php
Extra Packages for Enterprise Linux 9 - x86_64        2.3 MB/s |  11 MB     00:05
Remi's Modular repository for Enterprise Linux 9 - x8 2.9 kB/s | 833  B     00:00
Remi's Modular repository for Enterprise Linux 9 - x8 3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x478F8947:
 Userid     : "Remi's RPM repository (https://rpms.remirepo.net/) <remi@remirepo.net>"
 Fingerprint: B1AB F71E 14C9 D748 97E1 98A8 B195 27F1 478F 8947
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el9
Is this ok [y/N]: y
Remi's Modular repository for Enterprise Linux 9 - x8 1.4 MB/s | 754 kB     00:00
Safe Remi's RPM repository for Enterprise Linux 9 - x 2.9 kB/s | 833  B     00:00
Safe Remi's RPM repository for Enterprise Linux 9 - x 3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x478F8947:
 Userid     : "Remi's RPM repository (https://rpms.remirepo.net/) <remi@remirepo.net>"
 Fingerprint: B1AB F71E 14C9 D748 97E1 98A8 B195 27F1 478F 8947
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el9
Is this ok [y/N]: y
```

```bash
[welan@web ~]$ sudo dnf module enable php:remi-8.1 -y
Extra Packages for Enterprise Linux 9 - x86_64        2.4 MB/s |  11 MB     00:04
Remi's Modular repository for Enterprise Linux 9 - x8 3.4 kB/s | 833  B     00:00
Remi's Modular repository for Enterprise Linux 9 - x8 3.0 MB/s | 3.1 kB     00:00
```

```bash
[welan@web ~]$ sudo dnf install -y php81-php
Last metadata expiration check: 0:01:14 ago on Thu 17 Nov 2022 10:58:34 AM CET.
Dependencies resolved.
======================================================================================
 Package                         Arch      Version                 Repository    Size
======================================================================================
Installing:
 php81-php                       x86_64    8.1.12-1.el9.remi       remi-safe    1.7 M
Installing dependencies:
 checkpolicy
```

```bash
[welan@web ~]$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp --nogpgcheck
Last metadata expiration check: 0:14:59 ago on Thu 17 Nov 2022 10:58:34 AM CET.
Package libxml2-2.9.13-1.el9_0.1.x86_64 is already installed.
Package openssl-1:3.0.1-41.el9_0.x86_64 is already installed.
Package php81-php-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
```

```bash
[welan@web ~]$ wget https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
[welan@web ~]$ unzip nextcloud-25.0.0rc3.zip
[welan@web ~]$ sudo mv nextcloud /var/www/tp2_nextcloud/
[welan@web tp2_nextcloud]$ ls -al
total 4
drwxr-xr-x.  3 root  root    23 Nov 17 11:39 .
drwxr-xr-x.  5 root  root    54 Nov 17 11:27 ..
drwxr-xr-x. 14 welan welan 4096 Oct  6 14:47 nextcloud
[welan@web tp2_nextcloud]$ sudo chown -R apache nextcloud/
[welan@web tp2_nextcloud]$ ls -al
total 4
drwxr-xr-x.  3 root   root    23 Nov 17 11:39 .
drwxr-xr-x.  5 root   root    54 Nov 17 11:27 ..
drwxr-xr-x. 14 apache welan 4096 Oct  6 14:47 nextcloud
```

```bash
[welan@web ~]$ sudo vim /etc/httpd/conf.d/myconfig.conf
[sudo] password for welan:
```

On rajoute ceci : 
```bash
<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp2_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp2.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp2_nextcloud/> 
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

Et enfin on redemarre apache :
```bash
[welan@web ~]$ sudo systemctl stop httpd
[welan@web ~]$ sudo systemctl start httpd
```

On rajoute "10.102.1.11 web.tp2.linux" dans le fichier hosts de notre ordinateur
Et maintenant notre site est accessible

```bash
MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| nextcloud          |
| performance_schema |
+--------------------+
4 rows in set (0.001 sec)

MariaDB [(none)]> USE nextcloud;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [nextcloud]> SHOW TABLES;
+-----------------------------+
| Tables_in_nextcloud         |
+-----------------------------+
| oc_accounts                 |
| oc_accounts_data            |
| oc_activity                 |
| oc_activity_mq              |
| oc_addressbookchanges       |
| oc_addressbooks             |
| oc_appconfig                |
| oc_authorized_groups        |
| oc_authtoken                |
| oc_bruteforce_attempts      |
| oc_calendar_invitations     |
| oc_calendar_reminders       |
| oc_calendar_resources       |
| oc_calendar_resources_md    |
| oc_calendar_rooms           |
| oc_calendar_rooms_md        |
| oc_calendarchanges          |
| oc_calendarobjects          |
| oc_calendarobjects_props    |
| oc_calendars                |
| oc_calendarsubscriptions    |
| oc_cards                    |
| oc_cards_properties         |
| oc_circles_circle           |
| oc_circles_event            |
| oc_circles_member           |
| oc_circles_membership       |
| oc_circles_mount            |
| oc_circles_mountpoint       |
| oc_circles_remote           |
| oc_circles_share_lock       |
| oc_circles_token            |
| oc_collres_accesscache      |
| oc_collres_collections      |
| oc_collres_resources        |
| oc_comments                 |
| oc_comments_read_markers    |
| oc_dav_cal_proxy            |
| oc_dav_shares               |
| oc_direct_edit              |
| oc_directlink               |
| oc_federated_reshares       |
| oc_file_locks               |
| oc_file_metadata            |
| oc_filecache                |
| oc_filecache_extended       |
| oc_files_trash              |
| oc_flow_checks              |
| oc_flow_operations          |
| oc_flow_operations_scope    |
| oc_group_admin              |
| oc_group_user               |
| oc_groups                   |
| oc_jobs                     |
| oc_known_users              |
| oc_login_flow_v2            |
| oc_migrations               |
| oc_mimetypes                |
| oc_mounts                   |
| oc_notifications            |
| oc_notifications_pushhash   |
| oc_notifications_settings   |
| oc_oauth2_access_tokens     |
| oc_oauth2_clients           |
| oc_photos_albums            |
| oc_photos_albums_files      |
| oc_photos_collaborators     |
| oc_preferences              |
| oc_privacy_admins           |
| oc_profile_config           |
| oc_properties               |
| oc_ratelimit_entries        |
| oc_reactions                |
| oc_recent_contact           |
| oc_schedulingobjects        |
| oc_share                    |
| oc_share_external           |
| oc_storages                 |
| oc_storages_credentials     |
| oc_systemtag                |
| oc_systemtag_group          |
| oc_systemtag_object_mapping |
| oc_text_documents           |
| oc_text_sessions            |
| oc_text_steps               |
| oc_trusted_servers          |
| oc_twofactor_backupcodes    |
| oc_twofactor_providers      |
| oc_user_status              |
| oc_user_transfer_owner      |
| oc_users                    |
| oc_vcategory                |
| oc_vcategory_to_object      |
| oc_webauthn                 |
| oc_whats_new                |
+-----------------------------+
95 rows in set (0.000 sec)
```

On peut voir que Nextcloud a crée 95 tables
