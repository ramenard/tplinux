# 0 Préparation de la machine
```bash
[welan@localhost ~]$ ping 10.101.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=1.40 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.802 ms
64 bytes from 10.101.1.12: icmp_seq=3 ttl=64 time=0.808 ms
64 bytes from 10.101.1.12: icmp_seq=4 ttl=64 time=0.792 ms

--- 10.101.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
```
Les machines peuvent se ping

```bash
[welan@node1 ~]$ hostname
node1.tp1.b2
```

```bash
[welan@node2 ~]$ hostname
node2.tp1.b2
```

```bash
[welan@node2 ~]$ ping node1.tp1.b2
PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=1.28 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=2 ttl=64 time=0.845 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=3 ttl=64 time=0.830 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=4 ttl=64 time=0.971 ms

--- node1.tp1.b2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3008ms
```

```bash
[welan@node1 ~]$ dig ynov.com

;; ANSWER SECTION:
ynov.com.               206     IN      A       172.67.74.226
ynov.com.               206     IN      A       104.26.10.233
ynov.com.               206     IN      A       104.26.11.233

;; Query time: 26 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Mon Nov 14 11:48:26 CET 2022
;; MSG SIZE  rcvd: 85
```

```bash
[welan@node2 ~]$ dig ynov.com

;; ANSWER SECTION:
ynov.com.               232     IN      A       104.26.10.233
ynov.com.               232     IN      A       104.26.11.233
ynov.com.               232     IN      A       172.67.74.226

;; Query time: 26 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Mon Nov 14 11:50:32 CET 2022
;; MSG SIZE  rcvd: 85
```

```bash
[welan@node1 ~]$ sudo firewall-cmd --list-all

public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

# I Utilisateurs
## 1 Création et configuration

Pour créer un nouvel utilsateur : 
```bash
[welan@node1 ~]$ sudo useradd tresh --home /home/tresh -s /bin/bash
```

```bash
[welan@node1 ~]$ cat /etc/passwd
tresh:x:1001:1001::/home/tresh:/bin/bash
```
La partie "/home/tresh" correspond à son répertoire home et la partie "/bin/bash" au shell

Pour créer un nouveau groupe : 
```bash
sudo groupadd admins
```

```bash
sudo visudo /etc/sudoers
```
Puis ajouter la commande suivante pour lui donner les droits administrateur au groupe admins :
```bash
%admins  ALL=(ALL)       ALL
```  

```bash
sudo usermod -a -G admins tresh
```
Pour ajouter l'utilisateur tresh au groupe admin

Pour savoir si ça fonctionne on peut essayer d'accerder à un certain fichier : 
```bash
sudo cat /etc/shadow
```

## 2 SSH

Il faut d'abord créer une clé ssh du côté du poste client de l'administrateur
```bash
ssh-keygen -t rsa -b 4096
```

puis ensuite la transferer à notre vm :
```bash
ssh-copy-id tresh@10.101.1.11
```

```bash
ssh tresh@10.101.1.11
Last login: Mon Nov 14 12:59:21 2022
```

# II Partitionnement

```bash
[tresh@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    3G  0 disk
sdc           8:32   0    3G  0 disk
sr0          11:0    1 1024M  0 rom
```
On peut voir que sdb et sdc correspondent aux disques que l'on a crée 

D'abord on ajouter les disques en tant que Physical Volume dans LVM
```bash
[tresh@node1 ~]$ sudo pvcreate /dev/sdb
[sudo] password for tresh:
  Physical volume "/dev/sdb" successfully created.
[tresh@node1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
[tresh@node1 ~]$ sudo pvs
  Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VB19c59981-83061a07_ PVID 3tUMb3znbvaOY5cITUE18fThmXV91ABI last seen on /dev/sda2 not found.
  PV         VG Fmt  Attr PSize PFree
  /dev/sdb      lvm2 ---  3.00g 3.00g
  /dev/sdc      lvm2 ---  3.00g 3.00g
```

Ensuite on crée un groupe et on les ajoutes dedans
```bash
sudo vgcreate data /dev/sdb
[sudo] password for tresh:
  Volume group "data" successfully created
[tresh@node1 ~]$ sudo vgextend data /dev/sdc
  Volume group "data" successfully extended
[tresh@node1 ~]$ sudo vgs
  Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VB19c59981-83061a07_ PVID 3tUMb3znbvaOY5cITUE18fThmXV91ABI last seen on /dev/sda2 not found.
  VG   #PV #LV #SN Attr   VSize VFree
  data   2   0   0 wz--n- 5.99g 5.99g
```

Ensuite on va créer des logical volumes à partir du groupe data : 
```bash
[tresh@node1 ~]$ sudo lvcreate -L 1G data -n part1
  Logical volume "part1" created.
[tresh@node1 ~]$ sudo lvcreate -L 1G data -n part2
  Logical volume "part2" created.
[tresh@node1 ~]$ sudo lvcreate -L 1G data -n part3
  Logical volume "part3" created.
[tresh@node1 ~]$ sudo lvs
  Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VB19c59981-83061a07_ PVID 3tUMb3znbvaOY5cITUE18fThmXV91ABI last seen on /dev/sda2 not found.
  LV    VG   Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  part1 data -wi-a----- 1.00g
  part2 data -wi-a----- 1.00g
  part3 data -wi-a----- 1.00g
```

Ensuite on formate nos LV en ext4 (exemple pour part1 mais même chose pour part2 et part3):
```bash
[tresh@node1 ~]$ sudo mkfs -t ext4 /dev/data/part1
mke2fs 1.46.5 (30-Dec-2021)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: e4f561cf-a35c-4429-91db-e2e0b69babe9
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

Ensuite on monte ces partitions avec le point de montage que l'on veut
```bash
[tresh@node1 ~]$ sudo mount /dev/data/part1 /mnt/part1
[tresh@node1 ~]$ sudo mount /dev/data/part2 /mnt/part2
[tresh@node1 ~]$ sudo mount /dev/data/part3 /mnt/part3
[tresh@node1 ~]$ sudo mount
/dev/mapper/data-part1 on /mnt/part1 type ext4 (rw,relatime,seclabel)
/dev/mapper/data-part2 on /mnt/part2 type ext4 (rw,relatime,seclabel)
/dev/mapper/data-part3 on /mnt/part3 type ext4 (rw,relatime,seclabel)
[tresh@node1 ~]$ df -h
Filesystem              Size  Used Avail Use% Mounted on
/dev/mapper/data-part1  974M   24K  907M   1% /mnt/part1
/dev/mapper/data-part2  974M   24K  907M   1% /mnt/part2
/dev/mapper/data-part3  974M   24K  907M   1% /mnt/part3
```

Puis dans le dossier /etc/fstab on rajoute ces lignes suivantes pour que les partition soit montée automatiquement au démarrage du système
```bash
/dev/mapper/data-part1 /mnt/part1 ext4 defaults 0 0
/dev/mapper/data-part2 /mnt/part2 ext4 defaults 0 0
/dev/mapper/data-part3 /mnt/part3 ext4 defaults 0 0
```

# III Gestion de services
## 1 Interaction avec un service existant

```bash
[tresh@node1 ~]$ sudo systemctl is-active firewalld
[sudo] password for tresh:
active
```
Cette commande permet de savoir si l'unité firewalld est démarée

```bash
[tresh@node1 ~]$ sudo systemctl is-enabled firewalld
enabled
```
Cette commande permet de savoir si l'unité se lance au démarage

## 2 Création de service 
### A Unité simpliste 

```bash
[tresh@node1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
```
Cette commande nous permet d'ouvrir le port 8888

```bash
[welan@node2 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="afs/">afs/</a></li>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```
Grace à cette commande on peut voir que le serveur fonctionne

### B Modification de l'unité

```bash
[tresh@node1 ~]$ sudo useradd web --home /home/web -s /bin/bash
```
Pour créer l'utilisateur web

```bash
sudo usermod -a -G admins web
```
Pour l'ajouter au groupe admin

```bash
ls -l
total 0
drwxr-xr-x. 2 web root 17 Nov 14 20:00 meow
```
On peut voir ici les droits du dossier ansi que son propriétaire

```bash
[web@node1 ~]$ cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/var/www/meow

[Install]
WantedBy=multi-user.target
```
On ajoute les 2 dernieres ligne dans la partie "Service"

```bash
[welan@node2 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="cat">cat</a></li>
</ul>
<hr>
</body>
</html>
```
On peut voir ici que le serveur fonctionne