# Module 1 : Reverse Proxy
## II. Setup

La premiere chose à faire est d'installer nginx :
```bash
[welan@proxy ~]$ sudo dnf install nginx
Rocky Linux 9 - BaseOS                                8.3 kB/s | 3.6 kB     00:00
Rocky Linux 9 - BaseOS                                1.4 MB/s | 1.7 MB     00:01
Rocky Linux 9 - AppStream                             6.8 kB/s | 3.6 kB     00:00
Rocky Linux 9 - AppStream                             2.0 MB/s | 6.0 MB     00:03
Rocky Linux 9 - Extras                                5.6 kB/s | 2.9 kB     00:00
Dependencies resolved.
```

Ensuite de lancer le service nginx
```bash
[welan@proxy ~]$ sudo systemctl start nginx
[sudo] password for welan:
[welan@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: >
     Active: active (running) since Fri 2022-11-18 09:44:36 CET; 4s ago
    Process: 1061 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/S>
    Process: 1062 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 1063 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 1064 (nginx)
```

```bash
[welan@proxy ~]$ sudo ss -lntp
State      Recv-Q     Send-Q         Local Address:Port         Peer Address:Port     Process
LISTEN     0          128                  0.0.0.0:22                0.0.0.0:*         users:(("sshd",pid=705,fd=3))
LISTEN     0          511                  0.0.0.0:80                0.0.0.0:*         users:(("nginx",pid=1065,fd=6),("nginx",pid=1064,fd=6))
LISTEN     0          128                     [::]:22                   [::]:*         users:(("sshd",pid=705,fd=4))
LISTEN     0          511                     [::]:80                   [::]:*         users:(("nginx",pid=1065,fd=7),("nginx",pid=1064,fd=7))
```
On peut voir que le port d'écoute de nginx est le port 80 

```bash
[welan@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[welan@web ~]$ sudo firewall-cmd --reload
success
```
Maintenant le port est ouvert

```bash
[welan@proxy ~]$ ps -ef | grep nginx
root        1064       1  0 09:44 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1065    1064  0 09:44 ?        00:00:00 nginx: worker process
welan       1094     852  0 09:49 pts/0    00:00:00 grep --color=auto nginx
```
Nginx tourne sous l'utilisateur nginx

```bash
[welan@web ~]$ curl 10.102.1.13:80
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
```
On peut voir que le site est bien accessible

Pour transformer nginx en reverse proxy il faut d'abord créer un fichier de conf
Dans le fichier de conf principale il y a cette ligne :
```bash
 # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;
```

On crée un fichier .conf dans le dossier default.d en on y ajoute ceci :
```bash
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp2.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying 
        proxy_pass http://web.tp2.linux:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```

Ensuite dans le le fichier conf.php du dossier nextcloud on ajoute dans la partie array:
```bash
 'trusted_proxies'   => '10.102.1.13',
```

Dans le fichier hosts de la machine proxy on ajoute cette ligne :
```bash
10.102.1.11 web.tp2.linux
```

Puis dans le fichier host de notre pc :
```bash
10.102.1.13 web.tp2.linux
```

```bash
PS C:\Users\Raphaël> curl web.tp2.linux/


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html class="ng-csp" data-placeholder-focus="false" lang="en"
                    data-locale="en" >
```
On peut voir que le site est accessible

## III. HTTPS

La premiere chose à faire est de creer 2 clés :
```bash
[welan@proxy ~]$ openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout /etc/pki/nginx/private/privateKey.key -out /etc/pki/nginx/certificate.crt
.+......+............+...+....+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*...........+....+.....+.+..............+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
```

Ensuite dans le fichier de conf principal de nginx on enleve les lignes suivantes :
```bash
server {
        listen       80;
        listen       [::]:80;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
```

Puis dans le fichier de conf qu'on a crée plus tot dans le dossier conf.d de nginx 
On ajoute les chemins des clés et le nouveau port écouté
```bash
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp2.linux;

    # Port d'écoute de NGINX
    listen 443 ssl;

    ssl_certificate /etc/pki/nginx/certificate.crt;
    ssl_certificate_key /etc/pki/nginx/private/privateKey.key;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://web.tp2.linux:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```

Puis dans le dossier de conf de nextcloud on modifie la ligne suivante :
```bash
'overwrite.cli.url' => 'https://web.tp2.linux',
```

On oublie pas d'ouvrir le port du https donc le 443 sur la machine proxy :
```bash
[welan@web ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[welan@web ~]$ sudo firewall-cmd --reload
success
```

# Module 7 : Fail2Ban
La premiere chose à faire est d'installer fail2ban:
```bash
[welan@web ~]$ sudo dnf install fail2ban fail2ban-firewalld
```

Ensuite on démarre le service :
```bash
[welan@web ~]$ sudo systemctl start fail2ban
[welan@web ~]$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
[welan@web ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
```

On crée un nouveau fichier de conf pour le fail2ban :
```bash
[welan@web ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
```

Ensuite on modifie le nouveau fichier de conf sous la partie [DEFAULT] :
```bash
bantime = 3m
findtime = 1m
maxretry = 3
```

Ensuite on crée un nouveau fichier de conf propre à sshd :
```bash
[welan@web ~]$ sudo vim /etc/fail2ban/jail.d/sshd.local

[sshd]
enabled = true

# Override the default global configuration
# for specific jail sshd
bantime = 3m
findtime = 1m
maxretry = 3
```

Enfin on redémarre le service :
```bash
[welan@web ~]$ sudo systemctl restart fail2ban
```

Depuis une autre machine :
```bash
[welan@proxy ~]$ ssh welan@10.102.1.11
welan@10.102.1.11's password:
Permission denied, please try again.
welan@10.102.1.11's password:
Permission denied, please try again.
welan@10.102.1.11's password:
welan@10.102.1.11: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
```

Depuis la machine avec le fail2ban :
```bash
[welan@web ~]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     6
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 1
   |- Total banned:     2
   `- Banned IP list:   10.102.1.13
```

Pour deban l'ip voulu : 
```bash
[welan@web ~]$ sudo fail2ban-client unban 10.102.1.13
1
[welan@web ~]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     6
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 0
   |- Total banned:     2
   `- Banned IP list:
```

# Module 3 : Sauvegarde de base de données

On commence d'abord par créer un nouvel utilisateur qui sera utilisé pour faire les sauvegardes :
```bash
MariaDB [(none)]> CREATE USER 'dbsave'@'localhost' IDENTIFIED BY 'mysave';
Query OK, 0 rows affected (0.004 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 0 rows affected, 1 warning (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.004 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```

Ensuite on passe à la création du script :
```bash
mysqldump -u $user $database -h $host --password=$password > /srv/db_dumps/db_${database}_${currdate}.sql
```
Cette ligne permet de faire une sauvegarde de la db

```bash
tar -cf /srv/db_dumps/db_${database}_${currdate}.tar.gz /srv/db_dumps/db_${database}_${currdate}.sql
```
Cette ligne permet de compresser le fichier de sauvegarde :

```bash
rm /srv/db_dumps/db_${database}_${currdate}.sql
```
Cette ligne permet de supprimer le fichier initial de sauvegarde

```bash
#!/bin/bash
# Ecrit le 21/11/2022 par Raphaël Menard
# Le script sert a faire une sauvegarde de la base de donnée et de la compresser

user="dbsave"
database="nextcloud"
password="mysave"
currdate=$(date "+%Y%m%d%H%M%S")
host="localhost"

# création de la sauvegarde de la database
mysqldump -u $user $database -h $host --password=$password > /srv/db_dumps/db_${database}_${currdate}.sql

# compression du fichier de sauvegarde
tar -cf /srv/db_dumps/db_${database}_${currdate}.tar.gz /srv/db_dumps/db_${database}_${currdate}.sql

# on supprime la sauvegarde
rm /srv/db_dumps/db_${database}_${currdate}.sql
```

Ensuite on cré un nouvel utilisateur qui se chargera de lancer le fichier:
```bash
[welan@db srv]$ sudo useradd db_dumps --home /srv/db_dumps/ -s /usr/bin/nologin
[welan@db srv]$ sudo chown db_dumps db_dumps/
[welan@db srv]$ ls -l
total 4
drwxr-xr-x. 2 db_dumps root  48 Nov 18 15:59 db_dumps
-rwxr-xr-x. 1 root     root 610 Nov 19 15:47 tp3_db_dump.sh
[welan@db srv]$ sudo chown db_dumps tp3_db_dump.sh
```

```bash
[welan@db srv]$ sudo -u db_dumps /srv/tp3_db_dump.sh
tar: Removing leading `/' from member names
[welan@db srv]$ ls db_dumps/
db_nextcloud_20221118155912.tar.gz  db_nextcloud_20221119155349.tar.gz
```

On peut voir que le code a bien fonctionné 

## III. Service et timer

On crée le fichier db-dump.service dans le dossier /etc/systemd/system/
```bash
[Unit]
Description=run the dbsave

[Service]
ExecStart=sudo -u db_dumps /srv/tp3_db_dump.sh
Type=oneshot

[Install]
WantedBy=multi-user.target
```

```bash
[welan@db ~]$ sudo systemctl start db-dump
[welan@db ~]$ sudo systemctl status db-dump
○ db-dump.service - run the dbsave
     Loaded: loaded (/etc/systemd/system/db-dump.service; disabled; vendor preset: di>
     Active: inactive (dead)

Nov 19 16:08:25 db.tp2.linux systemd[1]: Starting run the dbsave...
Nov 19 16:08:25 db.tp2.linux sudo[14473]:     root : PWD=/ ; USER=db_dumps ; COMMAND=>
Nov 19 16:08:25 db.tp2.linux systemd[1]: db-dump.service: Deactivated successfully.
Nov 19 16:08:25 db.tp2.linux systemd[1]: Finished run the dbsave.
lines 1-8/8 (END)

[welan@db ~]$ ls /srv/db_dumps/
db_nextcloud_20221118155912.tar.gz  db_nextcloud_20221119160825.tar.gz
db_nextcloud_20221119155349.tar.gz
```

Puis on crée le fichier db-dump.timer au meme endroit que le fichier d'avant
```bash
[Unit]
Description=Run service X

[Timer]
OnCalendar=*-*-* 4:00:00

[Install]
WantedBy=timers.target
```

```bash
[welan@db ~]$ sudo systemctl start db-dump.timer
[welan@db ~]$ sudo systemctl enable db-dump.timer
Created symlink /etc/systemd/system/timers.target.wants/db-dump.timer → /etc/systemd/system/db-dump.timer.
[welan@db ~]$ sudo systemctl status db-dump.timer
● db-dump.timer - Run service X
     Loaded: loaded (/etc/systemd/system/db-dump.timer; enabled; vendor preset: disab>
     Active: active (elapsed) since Sat 2022-11-19 16:13:25 CET; 21s ago
      Until: Sat 2022-11-19 16:13:25 CET; 21s ago
    Trigger: n/a
   Triggers: ● db-dump.service

Nov 19 16:13:25 db.tp2.linux systemd[1]: Started Run service X.
lines 1-8/8 (END)

[welan@db ~]$ sudo systemctl list-timers
NEXT                        LEFT       LAST                        PASSED    UNIT    >
Sat 2022-11-19 17:05:55 CET 51min left Sat 2022-11-19 15:45:14 CET 28min ago dnf-make>
Sun 2022-11-20 00:00:00 CET 7h left    Sat 2022-11-19 15:45:14 CET 28min ago logrotat>
Sun 2022-11-20 15:45:14 CET 23h left   Sat 2022-11-19 15:45:14 CET 28min ago systemd->
n/a                         n/a        n/a                         n/a       db-dump.>

4 timers listed.
Pass --all to see loaded but inactive timers, too.
```

Pour tester j'ai modifié le timer pour faire une backup toute les 30s:
```bash
[Timer]
OnUnitActiveSec=30
```

On peut voir qu'il y une sauvegarde de plus
```bash
[welan@db ~]$ ls /srv/db_dumps/
db_nextcloud_20221118155912.tar.gz  db_nextcloud_20221119163848.tar.gz
db_nextcloud_20221119155349.tar.gz  db_nextcloud_20221120090736.tar.gz
db_nextcloud_20221119160825.tar.gz  db_nextcloud_20221120094810.tar.gz
[welan@db ~]$ ls /srv/db_dumps/
db_nextcloud_20221118155912.tar.gz  db_nextcloud_20221120090736.tar.gz
db_nextcloud_20221119155349.tar.gz  db_nextcloud_20221120094810.tar.gz
db_nextcloud_20221119160825.tar.gz  db_nextcloud_20221120094922.tar.gz
db_nextcloud_20221119163848.tar.gz
```

Pour restaurer la db il faut faire d'abrd unzip le fichier que l'on veut utiliser :
```bash
[welan@db ~]$ sudo tar -xf /srv/db_dumps/db_nextcloud_20221120104935.tar.gz
[welan@db ~]$ ls
db_nextcloud_20221120104935.sql
```

```bash
[welan@db ~]$ sudo mysql --binary-mode -u dbsave -p nextcloud < db_nextcloud_20221120104935.sql
```