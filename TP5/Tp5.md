# DjBot

Au niveau de la ligne "TOKEN" il faudra mettre votre Token pour votre bot discord dans le fichier config.json
```bash
    "TOKEN": "",
```

Ensuite pour lancer le Docker il faut faire :
```bash
docker built . -t evobot
```

Dans le script il faut rajouter le chemin de où se trouve le Dockerfile et le docker-compose.yml
```bash
DIRECTORY=""
```

Ensuite il faut rajouter notre script dans le crontab pour que le programme se lance toute les minutes
```bash
[welan@localhost bot]$ crontab -e
* * * * * /home/welan/bot/start_the_music.sh
```

Il suffit juste d'attendre que le script lance le container

Pour configurer le bot discord il faudra aller dans l'onglet bot et activer l'onglet 'Message Content Intent'

On peut donc maintenant utiliser le bot discord

# Netdata

Pour installer netdata :
```bash
curl https://my-netdata.io/kickstart.sh > /tmp/netdata-kickstart.sh && sh /tmp/netdata-kickstart.sh
```

Pour avoir des alertes discord de netdata il faut faire :
```bash
sudo /etc/netdata/edit-config health_alarm_notify.conf
```

Ensuite rajouter ceci à la fin de fichier :
```bash
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL=""

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
```

Dans la partie DISCORD_WEBHOOK_URL il faut rajouter le webhook du chanel discord dans lequel vous voulez vos alarmes

On peut aussi voir le monitoring directement sur l'url suivant :
```bash
http://10.104.1.10:19999/#after=-420;before=0;;theme=slate;utc=Europe%2FParis
```
