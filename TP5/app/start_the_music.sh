#!/bin/bash

# Set the name of the Docker container
CONTAINER_NAME="evobot"
DIRECTORY=""

# Check if the container is running
if ! docker ps | grep -q "$CONTAINER_NAME"; then
  # If not, start the container
  cd $DIRECTORY
  docker compose up -d
fi