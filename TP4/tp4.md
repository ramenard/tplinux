# I. Docker
## 1. Install

```bash
[welan@docker1 ~]$ sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo
[welan@docker1 ~]$ sudo dnf install docker-ce docker-ce-cli containerd.io
Docker CE Stable - x86_64                                                               2.3 kB/s |  12 kB     00:05
Last metadata expiration check: 0:00:05 ago on Thu 24 Nov 2022 10:41:42 AM CET.
Dependencies resolved.
```

Ensuite on lance docker :
```bash
[welan@docker1 ~]$ sudo systemctl start docker
[welan@docker1 ~]$ sudo systemctl status docker
● docker.service - Docker Application Container Engine
     Loaded: loaded (/usr/lib/systemd/system/docker.service; disabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-11-24 10:44:38 CET; 9s ago
TriggeredBy: ● docker.socket
```

Pour ajouter notre utilisateur au group docker
```bash
[welan@docker1 ~]$ sudo usermod -aG docker $(whoami)
```

Pour lancer nginx avec docker et les fichier de conf et html que l'on veut :
```bash
docker run --name web -v /home/welan/default/index.html:/usr/share/nginx/html/index.html -v /home/welan/default/servnginx.conf:/etc/nginx/conf.d/default.conf -m 500m --cpus="0.5" -p 8888:100 nginx
```

# II. Images
Voici mon fichier [Dockerfile](/TP4/Dockerfile)  
Pour que ça fonctionne il faut qu'on change la config de apache c'est pour cela que l'on envoie un fichier .conf
Voici ma conf d'apache:
```bash
Listen 80
LoadModule mpm_event_module "/usr/lib/apache2/modules/mod_mpm_event.so"
LoadModule dir_module "/usr/lib/apache2/modules/mod_dir.so"
LoadModule authz_core_module "/usr/lib/apache2/modules/mod_authz_core.so"
```

Ensuite on build notre fichier dockerfile:
```bash
[welan@docker1 default]$ docker build . -t apache
Sending build context to Docker daemon  6.144kB
Step 1/7 : FROM ubuntu
 ---> a8780b506fa4
Step 2/7 : RUN apt update -y
 ---> Using cache
 ---> 486f52e3986e
Step 3/7 : RUN apt install -y apache2
 ---> Using cache
 ---> 9a59b21c7a87
Step 4/7 : COPY ./index.html /var/www/html/index.html
 ---> Using cache
 ---> 582b42a0bb84
Step 5/7 : COPY ./apache.conf /etc/apache2/apache2.conf
 ---> Using cache
 ---> 7fb73efa2492
Step 6/7 : RUN useradd apachelol
 ---> Using cache
 ---> 7cd776cf41ba
Step 7/7 : RUN mkdir /etc/apache2/logs/
 ---> Running in 0c35a8581a2c
^[[ARemoving intermediate container 0c35a8581a2c
 ---> ca8c11621a82
Successfully built ca8c11621a82
Successfully tagged apache:latest
```
Puis ensuite on démarre notre docker 

```bash
[welan@docker1 default]$ docker run -p 8888:80 apache apache2 -DFOREGROUND
```
Comme on peut voir ici notre site fonctionne avec la page html que l'on crée sur notre machine
```bash
[welan@docker1 default]$ curl http://127.0.0.1:8888
en vrai flemme
```

# III. docker-compose
## 2. Make your own meow

```bash
RUN git clone https://gitlab.com/ramenard/hangmanlinux.git
```

Pour lancer le projet :
```bash
cd /TpLinux/TP4/app
docker build . -t groupie
docker compose up
```

```bash
[welan@localhost ~]$ curl 10.104.1.10:8888 | head -n 10
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="../static/style.css" rel="stylesheet" media="all" type="text/css">
  <title>Accueil</title>
</head>
```
